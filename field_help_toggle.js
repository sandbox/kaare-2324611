/**
 * @file
 * JavaScript logic for Field help toggle.
 *
 * This utility attacks a container which in turn contains the help text that
 * will be expandable.  To expand this text it's assumed you want to attach it
 * to a different selector within this very container. so in total three DOM
 * nodes is utilized:
 *
 * - The .field-help-toggle container.
 * - A selector for what will be toggled within this container.
 * - A selector for where to attach the toggle icon, also within this container.
 */

(function($, Drupal) {

Drupal.behaviors.fieldHelpToggle = {
  attach: function(context, settings) {
    if (settings.field_help_toggle == undefined) {
      return;
    }
    if (settings.field_help_toggle.field != undefined) {
      $(settings.field_help_toggle.field.wrapper, context).once('fht').each(function() {
        var field_name = Drupal.FieldHelpToggle.fieldName(this);
        var options = Drupal.settings.field_help_toggle.field;
        if (Drupal.settings.field_help_toggle.overrides[field_name] != undefined) {
          options = $.extend({}, options, Drupal.settings.field_help_toggle.overrides[field_name]);
        }
        $(this).data('fieldHelpToggle', new Drupal.FieldHelpToggle(this, options));
      });
    }
    if (settings.field_help_toggle.field_group != undefined) {
      $(settings.field_help_toggle.field_group.wrapper, context).once('fht').each(function() {
        $(this).data('fieldHelpToggle', new Drupal.FieldHelpToggle(this, Drupal.settings.field_help_toggle.field_group));
      });
    }
  }
};

/**
 * Logic around one expandable help text.
 */
Drupal.FieldHelpToggle = function(container, options) {
  $.extend(this, {
    // The outermost wrapper a button relates to.
    wrapper: '.fht-field',
    // jQuery selector for text that is expandable.
    target: '.fel-field-help-text',
    // jQuery selector for element the trigger icon should relate to.
    trigger_container: '.fel-field-label',
    // jQuery method for inserting the icon.
    trigger_insert_method: 'prepend'
  }, options);

  this.$target = $(this.target, container).first().hide().addClass('fht-hidden');
  this.createButton();
  if (this.trigger_container == this.wrapper) {
    // This is the outermost field container. Adding a selector for this makes no
    // sense. We're it.
    $(container)[this.trigger_insert_method](this.$button);
  }
  else {
    $(this.trigger_container, container).first()[this.trigger_insert_method](this.$button);
  }
  return this;
};

Drupal.FieldHelpToggle.prototype = {

  /**
   * Create a clickable button that expands or collapses the help text.
   */
  createButton: function() {
    this.$button = $(Drupal.theme('fieldHelpToggleButton'));
    var $target = this.$target;
    this.$button.click(function() {
      if ($target.hasClass('fht-hidden')) {
        $target.slideDown().removeClass('fht-hidden').addClass('fht-visible');
      }
      else {
        $target.slideUp().removeClass('fht-visible').addClass('fht-hidden');
      }
      return false;
    });
    return this.$button;
  }
};

/**
 * Find the machine readable field name of the given field wrapper.
 *
 * @param node
 *   DOM node containing field wrapper div.
 */
Drupal.FieldHelpToggle.fieldName = function (node) {
  var classes = node.className.split(/\s+/);
  for (var i in classes) {
    if (classes[i].substr(0, 11) == 'field-name-') {
      return classes[i].substr(11).replace(/-/g, '_');
    }
  }
  return false;
};

/**
 * Default theme implementation for field help text toggle button.
 */
Drupal.theme.prototype.fieldHelpToggleButton = function() {
  return '<img class="field-help-toggle-button" src="' + Drupal.settings.basePath + 'misc/help.png" alt="' + Drupal.t("Help icon") + '" title="' + Drupal.t("Show help for this field.") + '" />';
};

})(jQuery, Drupal);
