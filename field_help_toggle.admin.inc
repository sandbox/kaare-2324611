<?php

/**
 * @file
 * System settings for Field help toggle.
 */

/**
 * System settings form for Field help toggle.
 */
function field_help_toggle_settings_form() {
  $settings = field_help_toggle_field_settings();

  $form['field_help_toggle_field_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t("Field help text behavior"),
    '#description' => t("Set up default behavior for fields configured with expandable help text. These can be individually overridden per field instance."),
    '#tree' => TRUE,
  );
  $form['field_help_toggle_field_settings']['trigger_container'] = array(
    '#type' => 'radios',
    '#title' => t("Attach help icon to"),
    '#description' => t("Where in the field form should the toggle icon be placed. <em>Field label</em> will place it next to the configured label and <em>Help text</em> will place it next to the configured help text and serve as a replacement for the help text until the user clicks it. <em>Field container</em> is the outermost wrapper <code>&lt;div&gt;</code> for the field, including all items in multi value fields."),
    '#options' => array(
      '.fel-field-label' => t("Field label"),
      '.fel-field-help-text' => t("Field help text"),
      '.fht-field' => t("Field container"),
    ),
    '#default_value' => $settings['trigger_container'],
  );
  $form['field_help_toggle_field_settings']['trigger_insert_method'] = array(
    '#type' => 'select',
    '#title' => t("Method for attaching help button"),
    '#description' => t("How should the icon be attached relative to the element."),
    '#options' => array(
      'before' => t("Before"),
      'after' => t("After"),
      'prepend' => t("Inside, beginning"),
      'append' => t("Inside, end"),
    ),
    '#default_value' => $settings['trigger_insert_method'],
  );

  // Field group
  $group_settings = field_help_toggle_group_settings();
  $form['field_help_toggle_group_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t("Field group description behavior"),
    '#description' => t("Set up default behavior for fields groups configured with expandable help text. Not overridable."),
    '#tree' => TRUE,
  );
  $form['field_help_toggle_group_settings']['trigger_container'] = array(
    '#type' => 'radios',
    '#title' => t("Attach help icon to"),
    '#description' => t("Where in the field group should the toggle icon be placed. <em>Field group description</em> will place it next to the configured description and serve as a replacement for it until the user clicks the button. <em>Field group container</em> is the outermost wrapper <code>&lt;div&gt;</code> for the group."),
    '#options' => array(
      '.fel-field-group-description' => t("Field group description"),
      '.fht-field-group' => t("Field group container"),
    ),
    '#default_value' => $group_settings['trigger_container'],
  );
  $form['field_help_toggle_group_settings']['trigger_insert_method'] = array(
    '#type' => 'select',
    '#title' => t("Method for attaching help button"),
    '#description' => t("How should the icon be attached relative to the element."),
    '#options' => array(
      'before' => t("Before"),
      'after' => t("After"),
      'prepend' => t("Inside, beginning"),
      'append' => t("Inside, end"),
    ),
    '#default_value' => $group_settings['trigger_insert_method'],
  );

  return system_settings_form($form);
}
